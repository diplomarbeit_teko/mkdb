﻿using System;
using IdentityManager;
using IdentityManager.AspNetIdentity;
using IdentityManager.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using MKDB_v01.Models;

namespace MKDB_v01
{
    public partial class Startup
    {
        
        public void ConfigureAuth(IAppBuilder app)
        {
            // Konfigurieren des db-Kontexts, des Benutzer-Managers und des Anmelde-Managers für die Verwendung einer einzelnen Instanz pro Anforderung.
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            
            // Konfigurieren des Anmeldecookies.
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Aktiviert die Anwendung für die Überprüfung des Sicherheitsstempels, wenn sich der Benutzer anmeldet.
                    // Dies ist eine Sicherheitsfunktion, die verwendet wird, wenn Sie ein Kennwort ändern oder Ihrem Konto eine externe Anmeldung hinzufügen.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(10),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            

            //----->Bei einer Autentifizierung des IDM zu einem Späteren Zeitpunkt über ein Coookie
            //app.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions
            //{
            //    AuthenticationType = "Cookies"
            //});
            app.Map("/idm", idm =>
            {
                var factory = new IdentityManagerServiceFactory();

                factory.IdentityManagerService =
                    new Registration<IIdentityManagerService>(Create());

                idm.UseIdentityManager(new IdentityManagerOptions
                {
                    Factory = factory,
                    ////----> Cookie Autentifizierung
                    SecurityConfiguration = new HostSecurityConfiguration
                    {
                        HostAuthenticationType = "ApplicationCookie",
                        AdminRoleName = "IdentityManagerAdministrator",
                        RequireSsl = false
                    }

                });
            });


        }

        private IIdentityManagerService Create()
        {
            var context =
              new IdentityDbContext<IdentityUser>(
                @"DefaultConnection");

            var userStore = new UserStore<IdentityUser>(context);
            var userManager = new UserManager<IdentityUser>(userStore);

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var managerService =
              new AspNetIdentityManagerService<IdentityUser, string, IdentityRole, string>
                (userManager, roleManager);

            return managerService;
        }

    }
}