﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MKDB_v01.Startup))]
namespace MKDB_v01
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    
    }
}
