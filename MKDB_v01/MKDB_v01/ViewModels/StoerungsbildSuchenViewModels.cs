﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MKDB_v01.Models;

namespace MKDB_v01.ViewModels
{
    public class SucheViewModel

    {

        [MaxLength(50, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Freitext")]
        public string Freitext { get; set; }

        //----------------Dropdownfüllung-----------------
        public IEnumerable<Merkmal> Merkmale { get; set; }

        public IEnumerable<Fahrzeugtyp> Fahrzeugtypen { get; set; }

        public IEnumerable<Baugruppe> Baugruppen { get; set; }

        public IEnumerable<Bauteil> Bauteile { get; set; }

        //----------------Dropdown Resultate-----------------
        [Required(ErrorMessage = "Geben Sie mindestens einen Fahrzeugtypen an!")]
        [Display(Name = "Fahrzeugtypen *")]
        public int[] FahrzeugtypenGesetztIds { get; set; }

        [Display(Name = "Baugruppe")]
        public int? BaugruppeGesetztId { get; set; }

        [Display(Name = "Merkmale")]
        public int[] MerkmaleGesetztIds { get; set; }

        [Display(Name = "Bauteil")]
        public int? BauteilGesetztId { get; set; }



    }
    

    public class SuchresultateListViewModel
    {
        public IEnumerable<Stoerungsbild> Suchresultate { get; set; }   
    }

    public class EditViewModel
    {
        public Stoerungsbild Stoerungsbild { get; set; }

        [Display(Name = "Merkmale")]
        public int[] MerkmaleGesetztIds { get; set; }

        public IEnumerable<Merkmal> Merkmale { get; set; }
    }
}

