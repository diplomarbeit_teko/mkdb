﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MKDB_v01.ViewModels
{
    
    

    public class LoginViewModel
    {
        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [Display(Name = "Benutzername")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [DataType(DataType.Password)]
        [Display(Name = "Kennwort")]
        public string Password { get; set; }

        [Display(Name = "Speichern?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [Display(Name = "Benutzername")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [Display(Name = "Kürzel")]
        [StringLength(4, ErrorMessage = "\"{0}\" muss zwischen {2} und {1} Zeichen lang sein.", MinimumLength = 2)]
        public string Initialen { get; set; }

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [StringLength(100, ErrorMessage = "\"{0}\" muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Kennwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kennwort bestätigen")]
        [Compare("Password", ErrorMessage = "Das Kennwort entspricht nicht dem Bestätigungskennwort.")]
        public string ConfirmPassword { get; set; }
    }

    

    
}
