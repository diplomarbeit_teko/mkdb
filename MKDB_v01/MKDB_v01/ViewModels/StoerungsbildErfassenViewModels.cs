﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MKDB_v01.Models;

namespace MKDB_v01.ViewModels
{
    public class ErfassenViewModel

    {
        //----------------Dropdownfüllung-----------------
        public IEnumerable<Merkmal> Merkmale { get; set; }

        [Display(Name = "Fahrzeugtypen *")]
        public IEnumerable<Fahrzeugtyp> Fahrzeugtypen { get; set; }

        [Display(Name = "Baugruppe *")]
        public IEnumerable<Baugruppe> Baugruppen { get; set; }

        [Display(Name = "Bauteil *")]
        public IEnumerable<Bauteil> Bauteile { get; set; }

        //----------------Dropdown Resultate-----------------
        
        [Display (Name = "Fahrzeugtypen *")]
        public int[] FahrzeugtypenGesetztIds { get; set; }
        
        [Display(Name = "Merkmale")]
        public int[] MerkmaleGesetztIds { get; set; }
        
        //----------------Name des, zu erstellenden, Bauteils-----------------
        [MaxLength(50, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Neues Bauteil *")]
        public string NeuesBauteil { get; set; }
        
        public Stoerungsbild Stoerungsbild { get; set; }
    }
}
 
