﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MKDB_v01.Models;

namespace MKDB_v01.ViewModels
{
    

    

    public class ValidierenListViewModel
    {
        public IEnumerable<Stoerungsbild> Suchresultate { get; set; }
    }

    public class ValidierenViewModel
    {
        public Stoerungsbild Stoerungsbild { get; set; }

        [Display(Name = "Merkmale")]
        public int[] MerkmaleGesetztIds { get; set; }

        public IEnumerable<Merkmal> Merkmale { get; set; }
    }
}

    
