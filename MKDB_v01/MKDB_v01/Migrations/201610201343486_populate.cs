namespace MKDB_v01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populate : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (1,'Fahrwerk')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (2,'Antriebsanlage/Energieanlage')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (3,'Steuerungsanlage Fahrbetrieb')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (4,'Hilfsbetriebsanlage')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (5,'Ueberwachungs- und Sicherheitseinrichtungen')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (6,'Beleuchtung')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (7,'Klimatisierung')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (8,'Nebenbetriebsanlage')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (9,'T�ren')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (10,'Informationseinrichtungen')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (11,'Pneumatik/Hydraulik')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (12,'Bremse')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (13,'Fahrzeugverbindungseinrichtungen')");
            Sql("INSERT INTO Baugruppen(Id,BaugruppenName) VALUES (14,'Elektrische Leitungsverlegung')");



            Sql("INSERT INTO Fahrzeugtypen(Id,FahrzeugtypName) VALUES (1,'Combino XL')");
            Sql("INSERT INTO Fahrzeugtypen(Id,FahrzeugtypName) VALUES (2,'Combino VL')");
            Sql("INSERT INTO Fahrzeugtypen(Id,FahrzeugtypName) VALUES (3,'Combino Classic')");
            Sql("INSERT INTO Fahrzeugtypen(Id,FahrzeugtypName) VALUES (4,'VeVe 4/8')");



            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (1,'Fehlfunktion')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (2,'Funktionslos')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (3,'Display-Meldung')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (4,'Laermt/Vibriert')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (5,'Ueberhitzung')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (6,'Undichtheit')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (7,'Wartung')");
            Sql("INSERT INTO Merkmale(Id,MerkmalName) VALUES (8,'Umbau')");
        }
    
        
        public override void Down()
        {
        }
    }
}
