namespace MKDB_v01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Baugruppen",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        BaugruppenName = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Bauteile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BauteilName = c.String(nullable: false, maxLength: 64),
                        BaugruppeId = c.Int(nullable: false),
                        Baugruppe_Id = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Baugruppen", t => t.Baugruppe_Id, cascadeDelete: true)
                .Index(t => t.Baugruppe_Id);
            
            CreateTable(
                "dbo.Fahrzeugtypen",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        FahrzeugtypName = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stoerungsbilder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Stoerungstext = c.String(nullable: false, maxLength: 1000),
                        Beanstandung = c.String(nullable: false, maxLength: 1000),
                        Ursache = c.String(maxLength: 1000),
                        Abhilfe = c.String(maxLength: 2000),
                        IstNeu = c.Boolean(nullable: false),
                        ErfassungsDatum = c.DateTime(nullable: false),
                        Erfasser = c.String(nullable: false, maxLength: 30),
                        ValidationsDatum = c.DateTime(),
                        Validator = c.String(maxLength: 30),
                        BauteilId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bauteile", t => t.BauteilId, cascadeDelete: true)
                .Index(t => t.BauteilId);
            
            CreateTable(
                "dbo.Merkmale",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        MerkmalName = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FahrzeugtypBauteils",
                c => new
                    {
                        Fahrzeugtyp_Id = c.Byte(nullable: false),
                        Bauteil_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Fahrzeugtyp_Id, t.Bauteil_Id })
                .ForeignKey("dbo.Fahrzeugtypen", t => t.Fahrzeugtyp_Id, cascadeDelete: true)
                .ForeignKey("dbo.Bauteile", t => t.Bauteil_Id, cascadeDelete: true)
                .Index(t => t.Fahrzeugtyp_Id)
                .Index(t => t.Bauteil_Id);
            
            CreateTable(
                "dbo.MerkmalStoerungsbilds",
                c => new
                    {
                        Merkmal_Id = c.Byte(nullable: false),
                        Stoerungsbild_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Merkmal_Id, t.Stoerungsbild_Id })
                .ForeignKey("dbo.Merkmale", t => t.Merkmal_Id, cascadeDelete: true)
                .ForeignKey("dbo.Stoerungsbilder", t => t.Stoerungsbild_Id, cascadeDelete: true)
                .Index(t => t.Merkmal_Id)
                .Index(t => t.Stoerungsbild_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.MerkmalStoerungsbilds", "Stoerungsbild_Id", "dbo.Stoerungsbilder");
            DropForeignKey("dbo.MerkmalStoerungsbilds", "Merkmal_Id", "dbo.Merkmale");
            DropForeignKey("dbo.Stoerungsbilder", "BauteilId", "dbo.Bauteile");
            DropForeignKey("dbo.FahrzeugtypBauteils", "Bauteil_Id", "dbo.Bauteile");
            DropForeignKey("dbo.FahrzeugtypBauteils", "Fahrzeugtyp_Id", "dbo.Fahrzeugtypen");
            DropForeignKey("dbo.Bauteile", "Baugruppe_Id", "dbo.Baugruppen");
            DropIndex("dbo.MerkmalStoerungsbilds", new[] { "Stoerungsbild_Id" });
            DropIndex("dbo.MerkmalStoerungsbilds", new[] { "Merkmal_Id" });
            DropIndex("dbo.FahrzeugtypBauteils", new[] { "Bauteil_Id" });
            DropIndex("dbo.FahrzeugtypBauteils", new[] { "Fahrzeugtyp_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Stoerungsbilder", new[] { "BauteilId" });
            DropIndex("dbo.Bauteile", new[] { "Baugruppe_Id" });
            DropTable("dbo.MerkmalStoerungsbilds");
            DropTable("dbo.FahrzeugtypBauteils");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Merkmale");
            DropTable("dbo.Stoerungsbilder");
            DropTable("dbo.Fahrzeugtypen");
            DropTable("dbo.Bauteile");
            DropTable("dbo.Baugruppen");
        }
    }
}
