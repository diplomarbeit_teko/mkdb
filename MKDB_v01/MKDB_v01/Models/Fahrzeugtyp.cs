﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKDB_v01.Models
{
    public class Fahrzeugtyp
    {

        public byte Id { get; set; }

        [Display(Name = "Fahrzeugtyp")]
        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [MaxLength(64)]
        public string FahrzeugtypName { get; set; }

        //----------------Fremdschlüssel-----------------
        public IList<Bauteil> Bauteile { get; set; }


    }
}