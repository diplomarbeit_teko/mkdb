﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MKDB_v01.Models
{
    public class Stoerungsbild
    {

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [MaxLength(1000, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Störungstext")]
        [DataType(DataType.MultilineText)]
        public string Stoerungstext { get; set; }

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [MaxLength(1000, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Beanstandung")]
        [DataType(DataType.MultilineText)]
        public string Beanstandung { get; set; }

        [MaxLength(1000, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Ursache")]
        [DataType(DataType.MultilineText)]
        public string Ursache { get; set; }

        [MaxLength(2000, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        [Display(Name = "Abhilfe")]
        [DataType(DataType.MultilineText)]
        public string Abhilfe { get; set; }

        //----------------Automatisch generierte props-----------------
        public int Id { get; set; }

        [Required]
        [Display(Name = "Ist Neu")]
        public bool IstNeu { get; set; }

        [Required]
        [Display(Name = "Erfassungs-Datum")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
               ApplyFormatInEditMode = true)]
        public DateTime ErfassungsDatum { get; set; }

        [Required]
        [MaxLength(30, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        public string Erfasser { get; set; }

        [Display(Name = "Validations-Datum")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
               ApplyFormatInEditMode = true)]
        public DateTime? ValidationsDatum { get; set; }

        [MaxLength(30, ErrorMessage = "\"{0}\" darf maximal {1} Zeichen besitzen.")]
        public string Validator { get; set; }


        //----------------Fremdschlüssel-----------------
        [Required]
        public Bauteil Bauteil { get; set; }

        
        public int? BauteilId { get; set; }

        public IList<Merkmal> Merkmale { get; set; }

        
    }
}