﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MKDB_v01.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Baugruppe> Baugruppen { get; set; }
        public DbSet<Bauteil> Bauteile { get; set; }
        public DbSet<Fahrzeugtyp> Fahrzeugtypen { get; set; }
        public DbSet<Merkmal> Merkmale { get; set; }
        public DbSet<Stoerungsbild> Stoerungsbilder { get; set; }
        
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ApplicationUser>().ToTable("Benutzer").Property(p => p.Id).HasColumnName("BenutzerId");
            //modelBuilder.Entity<ApplicationUser>().Property(p => p.UserName).HasColumnName("Benutzername");

            //modelBuilder.Entity<IdentityUserRole>().ToTable("BenutzerRollen");
            //modelBuilder.Entity<IdentityUserRole>().Property(p => p.RoleId).HasColumnName("RollenId_FK");
            //modelBuilder.Entity<IdentityUserRole>().Property(p => p.UserId).HasColumnName("BenutzerId_FK");

            ////Wird hier nicht benötigt! Muss von ASPNET.Identity implementiert werden
            //modelBuilder.Entity<IdentityUserLogin>().ToTable("ExternLogins");


            //modelBuilder.Entity<IdentityUserClaim>().ToTable("BenutzerClaims").Property(p => p.Id).HasColumnName("ClaimId");
            //modelBuilder.Entity<IdentityUserClaim>().Property(p => p.UserId).HasColumnName("BenutzerId_FK");
            //modelBuilder.Entity<IdentityUserClaim>().Property(p => p.ClaimType).HasColumnName("ClaimTyp");
            //modelBuilder.Entity<IdentityUserClaim>().Property(p => p.ClaimValue).HasColumnName("ClaimWert");

            //modelBuilder.Entity<IdentityRole>().ToTable("Rollen").Property(p => p.Id).HasColumnName("RollenId");

            modelBuilder.Entity<Baugruppe>().ToTable("Baugruppen");
            modelBuilder.Entity<Bauteil>().ToTable("Bauteile");
            modelBuilder.Entity<Fahrzeugtyp>().ToTable("Fahrzeugtypen");
            modelBuilder.Entity<Merkmal>().ToTable("Merkmale");

            modelBuilder.Entity<Stoerungsbild>().ToTable("Stoerungsbilder");




        }
    }
}