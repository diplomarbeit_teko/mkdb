﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKDB_v01.Models
{
    public class Bauteil
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "\"{0}\" ist erforderlich!")]
        [MaxLength(64)]
        [Display(Name = "Bauteil")]
        public string BauteilName { get; set; }


        //----------------Fremdschlüssel-----------------
        [Required]
        [Display(Name = "Baugruppe")]
        public Baugruppe Baugruppe { get; set; }

        [Required]
        [Display(Name = "Baugruppe")]
        public int BaugruppeId { get; set; }

        [Required]
        public IList<Fahrzeugtyp> Fahrzeugtypen { get; set; }

        public IList<Stoerungsbild> Stoerungsbilder { get; set; }
    }
}