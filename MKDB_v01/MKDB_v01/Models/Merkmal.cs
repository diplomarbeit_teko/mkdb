﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKDB_v01.Models
{
    public  class Merkmal
    {
        public byte Id { get; set; }

        [Required]
        [MaxLength(64)]
        public string MerkmalName { get; set; }

        //----------------Fremdschlüssel-----------------
        public IList<Stoerungsbild> Stoerungsbilder { get; set; }   
    }
}