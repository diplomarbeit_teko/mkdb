﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace MKDB_v01.Controllers
{
    public class AdminController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, string returnUrl)
        {
            if (username == "Admin" && password == "Admin@1234")
            {
                var claims = new Claim[]
                {
                    new Claim("name", "Admin"),
                    new Claim("role", "Superuser"),
                };

                var id = new ClaimsIdentity("Cookies");
                Request.GetOwinContext().Authentication.SignIn(id);
                return Redirect(returnUrl);
            }
            return View();
        }
    }
}