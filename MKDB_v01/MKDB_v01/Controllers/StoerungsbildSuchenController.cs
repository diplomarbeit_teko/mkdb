﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MKDB_v01.Models;
using MKDB_v01.ViewModels;

namespace MKDB_v01.Controllers
{
    public class StoerungsbildSuchenController : Controller
    {

        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        // GET: StoerungsbildSuchen
        public ActionResult Suche()
        {
            var baugruppen = _context.Baugruppen.ToList();
            var fahrzeugtypen = _context.Fahrzeugtypen.ToList();
            var merkmale = _context.Merkmale.ToList();
            var bauteile = _context.Bauteile.ToList();

            var viewModel = new SucheViewModel()
            {
                Baugruppen = baugruppen,
                Fahrzeugtypen = fahrzeugtypen,
                Merkmale = merkmale,
                Bauteile = bauteile

            };
            return View(viewModel);
        }

        // GET: StoerungsbildSuchen/Details/5
        public ActionResult Details(int id)
        {
            try
            {


                Stoerungsbild stoerungsbild = _context.Stoerungsbilder
                    .Include(s => s.Bauteil)
                    .Include(s => s.Bauteil.Fahrzeugtypen)
                    .Include(s => s.Merkmale)
                    .Include(s => s.Bauteil.Baugruppe)
                    .Single(s => s.Id == id);

                return View("Details", stoerungsbild);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
                
                return View("Error");
            }

            
        }

        // GET: StoerungsbildSuchen/StoerungsbilderAusDbLesen
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StoerungsbilderAusDbLesen(SucheViewModel model)
        {
            //_context = new ApplicationDbContext();
            // Minimal muss der Fahrzeugtyp angegeben werden
            if (!ModelState.IsValid)
            {
                model.Merkmale = _context.Merkmale.ToList();
                model.Baugruppen = _context.Baugruppen.ToList();
                model.Fahrzeugtypen = _context.Fahrzeugtypen.ToList();
                model.Bauteile = _context.Bauteile.ToList();
                    

                return View("Suche", model);
            }

            IEnumerable<Stoerungsbild> sb;

            if (model.BauteilGesetztId == 0 || model.BauteilGesetztId == null)
            {
                //holt alle störungsbilder aus der BD die einem der angegebenen Fahrzeugtypen zugeordnet sind
                sb = _context.Stoerungsbilder
                    .Include(s => s.Bauteil)
                    .Include(s => s.Bauteil.Baugruppe)
                    .Include(s => s.Merkmale)
                    .Include(s => s.Bauteil.Fahrzeugtypen)
                    .Where(
                        s =>
                            s.Bauteil.Fahrzeugtypen.Any(
                                l => s.IstNeu == false && model.FahrzeugtypenGesetztIds.Contains(l.Id))).ToList();
            }
            

            else
            {
                sb = _context.Stoerungsbilder
                    .Include(s => s.Bauteil)
                    .Include(s => s.Bauteil.Baugruppe)
                    .Include(s => s.Merkmale)
                    .Include(s => s.Bauteil.Fahrzeugtypen)
                    .Where(
                        s => s.IstNeu == false && s.Bauteil.Id == model.BauteilGesetztId).ToList();
            }
            
            
            if (model.Freitext != null)
            {
                sb = sb.Where(a => a.Stoerungstext.ToLower().Contains(model.Freitext.ToLower()) | a.Beanstandung.ToLower().Contains(model.Freitext.ToLower()));

            }
            
            if (model.BaugruppeGesetztId != null)
            {
                sb = sb.Where(a => a.Bauteil.Baugruppe.Id == model.BaugruppeGesetztId).ToList();

            }

            if (model.MerkmaleGesetztIds != null)
            {
                sb = sb.Where(s => s.Merkmale.Any(m => model.MerkmaleGesetztIds.Contains(m.Id))).ToList();


            }

            SuchresultateListViewModel returnModel = new SuchresultateListViewModel
            {
                Suchresultate = sb
            };


            return View("List", returnModel);
        }


        // GET: StoerungsbildSuchen/Edit/5
        [Authorize(Roles = "Superuser")]
        public ActionResult Edit(int id)
        {
            try
            {


                if (id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                EditViewModel model = new EditViewModel();

                model.Stoerungsbild = _context.Stoerungsbilder
                    .Include(s => s.Bauteil)
                    .Include(s => s.Bauteil.Fahrzeugtypen)
                    .Include(s => s.Merkmale)
                    .Include(s => s.Bauteil.Baugruppe)
                    .Single(s => s.Id == id);

                if (model.Stoerungsbild == null)
                {
                    return HttpNotFound();
                }

                model.Merkmale = _context.Merkmale;

                return View("Edit", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);


                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superuser")]
        public ActionResult Edit(EditViewModel model)
        {
            Stoerungsbild sb = _context.Stoerungsbilder
                .Include(s => s.Bauteil)
                .Include(s => s.Bauteil.Fahrzeugtypen)
                .Include(s => s.Merkmale)
                .Include(s => s.Bauteil.Baugruppe)
                .Single(s => s.Id == model.Stoerungsbild.Id);

            sb.Stoerungstext = model.Stoerungsbild.Stoerungstext;
            sb.Beanstandung = model.Stoerungsbild.Beanstandung;
            sb.Abhilfe = model.Stoerungsbild.Abhilfe;
            sb.Ursache = model.Stoerungsbild.Ursache;

            //Gesetzte Merkmale aus der DB lesen und im Model Speichern
            if (model.MerkmaleGesetztIds != null)
            {
                sb.Merkmale = new List<Merkmal>();

                foreach (int i in model.MerkmaleGesetztIds)
                {
                    var mm = _context.Merkmale.Single(m => m.Id == i);

                    sb.Merkmale.Add(mm);
                }

            }

            //Validator setzen & Istneu entfernen
            sb.ValidationsDatum = DateTime.Today;
            sb.Validator = User.Identity.Name;

            sb.IstNeu = false;


            try
            {
                _context.Entry(sb).State = EntityState.Modified;
                _context.SaveChanges();
                return View("Erfolg");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Edit fehlgeschlagen, versuchen Sie es erneut und benachrichtigen Sie den Systemadministrator.");
                return View("Edit", model);
            }



        }

        // GET: StoerungsbildSuchen/Delete/5
        [Authorize(Roles = "Superuser")]
        public ActionResult Loeschen(int id)


        {
            try
            {


                Stoerungsbild stoerungsbild = _context.Stoerungsbilder
                    .Include(s => s.Bauteil)
                    .Include(s => s.Bauteil.Fahrzeugtypen)
                    .Include(s => s.Merkmale)
                    .Include(s => s.Bauteil.Baugruppe)
                    .Single(s => s.Id == id);



                return View("Loeschen", stoerungsbild);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View("Error");
            }
        }

        // POST: StoerungsbildSuchen/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superuser")]
        public ActionResult StoerungsbildLoeschen(Stoerungsbild sb)
        {
            try
            {
                Stoerungsbild stoerungsbild = _context.Stoerungsbilder.Find(sb.Id);
                _context.Stoerungsbilder.Remove(stoerungsbild);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View("Error");
            }

            return View("Erfolg");
        }


        public JsonResult Bauteile(int? baugruppeId, int?[] fahrzeugtypenGesetztIds)  //Bauteile für das Dropdown aus der Datenbank laden
        {


            IEnumerable<Bauteil> bt =
                _context.Bauteile.Where(
                    b =>
                        b.Baugruppe.Id == baugruppeId &&
                        b.Fahrzeugtypen.Any(ft => fahrzeugtypenGesetztIds.Contains(ft.Id)));



            return Json(bt, JsonRequestBehavior.AllowGet); //Gibt die gefundenen Bauteile in einem Json zurück
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
