﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MKDB_v01.Models;
using MKDB_v01.ViewModels;

namespace MKDB_v01.Controllers
{
    [Authorize(Roles = "Superuser")]
    public class StoerungsbilderValidierenController : Controller
    {

        private ApplicationDbContext _context = new ApplicationDbContext();

        // GET: StoerungsbilderValidieren
        public ActionResult ValidierenList()
        {
            ValidierenListViewModel sbList = new ValidierenListViewModel();

            sbList.Suchresultate = _context.Stoerungsbilder
                .Include(s => s.Bauteil)
                .Include(s => s.Bauteil.Baugruppe)
                .Include(s => s.Merkmale)
                .Include(s => s.Bauteil.Fahrzeugtypen)
                .Where(s => s.IstNeu == true);



            return View(sbList);
        }


        public ActionResult Validieren(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValidierenViewModel model = new ValidierenViewModel();

            model.Stoerungsbild = _context.Stoerungsbilder
                .Include(s => s.Bauteil)
                .Include(s => s.Bauteil.Fahrzeugtypen)
                .Include(s => s.Merkmale)
                .Include(s => s.Bauteil.Baugruppe)
                .Single(s => s.Id == id);

            if (model.Stoerungsbild == null)
            {
                return HttpNotFound();
            }

            model.Merkmale = _context.Merkmale;
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Validieren(EditViewModel model)
        {
            Stoerungsbild sb = _context.Stoerungsbilder
                .Include(s => s.Bauteil)
                .Include(s => s.Bauteil.Fahrzeugtypen)
                .Include(s => s.Merkmale)
                .Include(s => s.Bauteil.Baugruppe)
                .Single(s => s.Id == model.Stoerungsbild.Id);

            sb.Stoerungstext = model.Stoerungsbild.Stoerungstext;
            sb.Beanstandung = model.Stoerungsbild.Beanstandung;
            sb.Abhilfe = model.Stoerungsbild.Abhilfe;
            sb.Ursache = model.Stoerungsbild.Ursache;

            //Gesetzte Merkmale aus der DB lesen und im Model Speichern
            if (model.MerkmaleGesetztIds != null)
            {
                sb.Merkmale = new List<Merkmal>();

                foreach (int i in model.MerkmaleGesetztIds)
                {
                    var mm = _context.Merkmale.Single(m => m.Id == i);

                    sb.Merkmale.Add(mm);
                }

            }

            //Validator setzen & Istneu entfernen
            sb.ValidationsDatum = DateTime.Today;
            sb.Validator = User.Identity.Name;

            sb.IstNeu = false;


            try
            {
                _context.Entry(sb).State = EntityState.Modified;
                _context.SaveChanges();
                return RedirectToAction("ValidierenList");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
                ModelState.AddModelError("",
                    "Validation fehlgeschlagen, versuchen Sie es erneut und benachrichtigen Sie den Systemadministrator.");
                return View(model);
            }



        }

        

        public ActionResult Loeschen(int id)

        {
            Stoerungsbild stoerungsbild = _context.Stoerungsbilder
                .Include(s => s.Bauteil)
                .Include(s => s.Bauteil.Fahrzeugtypen)
                .Include(s => s.Merkmale)
                .Include(s => s.Bauteil.Baugruppe)
                .Single(s => s.Id == id);



            return View("Loeschen", stoerungsbild);
        }

        // POST: StoerungsbildSuchen/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StoerungsbildLoeschen(Stoerungsbild sb)
        {
            try
            {
                Stoerungsbild stoerungsbild = _context.Stoerungsbilder.Find(sb.Id);
                _context.Stoerungsbilder.Remove(stoerungsbild);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            ModelState.AddModelError("", "Das Störungsbild wurde gelöscht");
            return RedirectToAction("ValidierenList");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }


}