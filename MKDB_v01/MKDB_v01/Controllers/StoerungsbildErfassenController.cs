﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MKDB_v01.Models;
using MKDB_v01.ViewModels;

namespace MKDB_v01.Controllers
{
    [Authorize(Roles = "Superuser,Writer")]
    public class StoerungsbildErfassenController : Controller
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        // GET: StörungsbildErfassen
        public ActionResult Erfassen()
        {
            var baugruppen = _context.Baugruppen.ToList();
            var fahrzeugtypen = _context.Fahrzeugtypen.ToList();
            var merkmale = _context.Merkmale.ToList();
            var bauteile = _context.Bauteile.ToList();

            var viewModel = new ErfassenViewModel
            {
                Baugruppen = baugruppen,
                Fahrzeugtypen = fahrzeugtypen,
                Merkmale = merkmale,
                Bauteile = bauteile
            };

            ViewBag.var3 = bauteile;

            return View(viewModel);
        }


        // POST: StörungsbildErfassen/Erfassen
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StoerungsbildInDbSchreiben(ErfassenViewModel model)
        {
            ModelState["Stoerungsbild.Erfasser"].Errors.Clear();

            if (((model.Stoerungsbild.BauteilId != null) && (model.Stoerungsbild.BauteilId != 0)) ||
                (model.NeuesBauteil != null))
                ModelState["Stoerungsbild.Bauteil.BauteilName"].Errors.Clear();

            if (!((model.Stoerungsbild.BauteilId == null) || (model.Stoerungsbild.BauteilId == 0)) &&
                (model.NeuesBauteil != null))
                ModelState.AddModelError("error",
                    "Es wurde ein neues Bauteil und ein bestehendes Bauteil angegeben. Bitte nur eine Funktion verwenden!");


            if (model.FahrzeugtypenGesetztIds != null)
                ModelState["Stoerungsbild.Bauteil.Fahrzeugtypen"].Errors.Clear();
            if (model.Stoerungsbild.Bauteil.BaugruppeId != 0)
                ModelState["Stoerungsbild.Bauteil.Baugruppe"].Errors.Clear();

            //--------------------->> Validierung für nur ein Bauteil fehlt noch

            if (!ModelState.IsValid)
            {
                model.Merkmale = _context.Merkmale.ToList();
                model.Baugruppen = _context.Baugruppen.ToList();
                model.Fahrzeugtypen = _context.Fahrzeugtypen.ToList();
                model.Bauteile = _context.Bauteile.ToList();
                model.Stoerungsbild.BauteilId = 0;

                return View("Erfassen", model);
            }


            model.Stoerungsbild.Erfasser = User.Identity.Name;
            model.Stoerungsbild.ErfassungsDatum = DateTime.Today;

            // wenn der User Supeuser ist, wird er als Validator angegeben
            if (User.IsInRole("Superuser"))
            {
                model.Stoerungsbild.IstNeu = false;
                model.Stoerungsbild.Validator = User.Identity.Name;
                model.Stoerungsbild.ValidationsDatum = DateTime.Today;
            }
            // Reader wird das Störungsbild als neu markiert
            else
            {
                model.Stoerungsbild.IstNeu = true;
            }

            //wenn ein neues Bauteil angegeben wurde, wird dieses erst erstellt
            if (model.NeuesBauteil != null)
            {
                model.Stoerungsbild.Bauteil.Fahrzeugtypen = new List<Fahrzeugtyp>();

                foreach (var i in model.FahrzeugtypenGesetztIds)
                {
                    var typ = _context.Fahrzeugtypen.Single(f => f.Id == i);

                    model.Stoerungsbild.Bauteil.Fahrzeugtypen.Add(typ);
                }

                model.Stoerungsbild.Bauteil.Baugruppe =
                    _context.Baugruppen.Single(b => b.Id == model.Stoerungsbild.Bauteil.BaugruppeId);

                model.Stoerungsbild.Bauteil.BauteilName = model.NeuesBauteil;

                //Neues Bauteil wird in die DB geschrieben
                _context.Bauteile.Add(model.Stoerungsbild.Bauteil);
                //await _context.SaveChangesAsync();
            }

            //wenn kein neues Bauteil angegeben wurde, wird es aus der DB geladen
            else if ((model.NeuesBauteil == null) &&
                     ((model.Stoerungsbild.BauteilId != null) & (model.Stoerungsbild.BauteilId != 0)))
            {
                model.Stoerungsbild.Bauteil = _context.Bauteile.Single(b => b.Id == model.Stoerungsbild.BauteilId);
            }


            // Merkmale hinzufügen
            if (model.MerkmaleGesetztIds != null)
            {
                model.Stoerungsbild.Merkmale = new List<Merkmal>();

                foreach (var i in model.MerkmaleGesetztIds)
                {
                    var mm = _context.Merkmale.Single(m => m.Id == i);

                    model.Stoerungsbild.Merkmale.Add(mm);
                }
            }

            try
            {
                _context.Stoerungsbilder.Add(model.Stoerungsbild);
                await _context.SaveChangesAsync();

                return View("Erfolg");
            }
            // irgendwas ist schiefgegangen
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View("Fehlgeschlagen");
            }
        }

        public JsonResult Bauteile(int? baugruppeId, int?[] fahrzeugtypenGesetztIds)
            //fetch the team details from database
        {
            

            IEnumerable<Bauteil> bt =
                _context.Bauteile.Where(
                    b =>
                        (b.Baugruppe.Id == baugruppeId) &&
                        b.Fahrzeugtypen.Any(ft => fahrzeugtypenGesetztIds.Contains(ft.Id)));


            return Json(bt, JsonRequestBehavior.AllowGet); //return the list objects in json form
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
            base.Dispose(disposing);
        }
    }
}